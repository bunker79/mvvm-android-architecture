package com.bunker79.mvvm.ui.main;

import com.bunker79.mvvm.api.ApiService;
import com.bunker79.mvvm.model.PostModel;
import com.bunker79.mvvm.model.response.ListResponse;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import rx.android.schedulers.AndroidSchedulers;

public class ListDataViewModel extends ViewModel {

    private MutableLiveData<ListResponse> mListResponse;

    public LiveData<ListResponse> getResponse() {

        if (mListResponse == null)
            mListResponse = new MutableLiveData<>();

        ApiService.INSTANCE.getApiInterface()
                .getPosts()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onLoaded, this::onError);

        return mListResponse;
    }

    private void onLoaded(List<PostModel> posts) {
        ListResponse response = new ListResponse();
        response.setList(posts);

        mListResponse.setValue(response);
    }

    private void onError(Throwable throwable) {
        ListResponse response = new ListResponse();
        response.setThrowable(throwable);

        mListResponse.setValue(response);
    }
}
