package com.bunker79.mvvm.ui.edit_post;

import com.bunker79.mvvm.api.ApiService;
import com.bunker79.mvvm.model.PostModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import rx.android.schedulers.AndroidSchedulers;

public class EditPostViewModel extends ViewModel {

    public enum Status {
        TITLE_EMPTY,
        BODY_EMPTY,
        EDIT_SUCCESS,
        EDIT_ERROR
    }

    private MutableLiveData<Status> mStatus;

    public LiveData<Status> getStatus() {
        if (mStatus == null)
            mStatus = new MutableLiveData<>();

        return mStatus;
    }

    void save(int user_id, int post_id, String title, String body) {
        if (title == null || title.isEmpty())
            mStatus.setValue(Status.TITLE_EMPTY);
        else if (body == null || body.isEmpty())
            mStatus.setValue(Status.BODY_EMPTY);
        else
            ApiService.INSTANCE.getApiInterface().savePost(post_id, new PostModel(user_id, post_id, title, body))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::onEditSuccess, this::onEditError);
    }

    private void onEditSuccess(PostModel postModel) {
        mStatus.setValue(Status.EDIT_SUCCESS);
    }

    private void onEditError(Throwable throwable) {
        mStatus.setValue(Status.EDIT_ERROR);
    }
}
