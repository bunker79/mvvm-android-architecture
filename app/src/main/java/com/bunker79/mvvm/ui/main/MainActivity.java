package com.bunker79.mvvm.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.bunker79.mvvm.R;
import com.bunker79.mvvm.model.response.ListResponse;
import com.bunker79.mvvm.ui.adapters.ListAdapter;
import com.bunker79.mvvm.ui.edit_post.EditPostActivity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recycler)
    RecyclerView mRecycler;
    @BindView(R.id.retryLayout)
    LinearLayout mRetryLayout;
    @BindView(R.id.loadingLayout)
    FrameLayout mLoadingLayout;

    private ListDataViewModel mListDataViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mListDataViewModel = ViewModelProviders.of(this).get(ListDataViewModel.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
        load();
    }

    private void load() {
        mLoadingLayout.setVisibility(View.VISIBLE);
        mRetryLayout.setVisibility(View.GONE);
        mListDataViewModel.getResponse().observe(this, this::onDataChanged);
    }

    private void onDataChanged(ListResponse response) {

        if (response.getThrowable() != null) {
            mRetryLayout.setVisibility(View.VISIBLE);
            mLoadingLayout.setVisibility(View.GONE);
            return;
        }

        mRecycler.setLayoutManager(new LinearLayoutManager(this));
        mRecycler.setAdapter(new ListAdapter(response.getList(), post -> {
            if (post != null) {
                Intent intent = new Intent(MainActivity.this, EditPostActivity.class);
                intent.putExtra("post", post);
                startActivity(intent);
            }
        }));

        mRetryLayout.setVisibility(View.GONE);
        mLoadingLayout.setVisibility(View.GONE);
    }

    @OnClick(R.id.btnRetry)
    protected void onRetry() {
        mRetryLayout.setVisibility(View.GONE);
        mLoadingLayout.setVisibility(View.VISIBLE);
        load();
    }
}
