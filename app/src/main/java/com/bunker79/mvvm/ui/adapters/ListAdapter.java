package com.bunker79.mvvm.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bunker79.mvvm.R;
import com.bunker79.mvvm.databinding.VhItemBinding;
import com.bunker79.mvvm.model.PostModel;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class ListAdapter extends RecyclerView.Adapter {

    private static final int TYPE_EMPTY = 0;
    private static final int TYPE_ITEM = 1;

    private List<PostModel> mSource;
    private OnItemClickListener mListener;

    public ListAdapter(List<PostModel> source, OnItemClickListener listener) {
        mSource = source;
        mListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (viewType) {
            case TYPE_EMPTY:
                view = inflater.inflate(R.layout.vh_list_empty, parent, false);
                return new EmptyHolder(view);
            default:
                view = inflater.inflate(R.layout.vh_item, parent, false);
                return new ItemHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemHolder) {
            ItemHolder vh = (ItemHolder) holder;
            vh.setup(mSource.get(position));
            vh.mRoot.setOnClickListener(view -> {
                if (view.getTag() != null && mListener != null)
                    mListener.onItemClick((PostModel) view.getTag());
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mSource != null && !mSource.isEmpty() ? TYPE_ITEM : TYPE_EMPTY;
    }

    @Override
    public int getItemCount() {
        return mSource != null && !mSource.isEmpty() ? mSource.size() : 1;
    }

    private class EmptyHolder extends RecyclerView.ViewHolder {
        EmptyHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

    private class ItemHolder extends RecyclerView.ViewHolder {

        private VhItemBinding mBinding;

        private View mRoot;

        ItemHolder(@NonNull View itemView) {
            super(itemView);
            mBinding = DataBindingUtil.bind(itemView);
            mRoot = itemView.findViewById(R.id.root);
        }

        private void setup(PostModel post) {
            mBinding.setPost(post);
            mBinding.executePendingBindings();
            mRoot.setTag(post);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(PostModel post);
    }
}
