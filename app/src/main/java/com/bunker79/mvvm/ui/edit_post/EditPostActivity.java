package com.bunker79.mvvm.ui.edit_post;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.bunker79.mvvm.R;
import com.bunker79.mvvm.databinding.ActivityEditPostBinding;
import com.bunker79.mvvm.model.PostModel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditPostActivity extends AppCompatActivity {

    @BindView(R.id.editTitle)
    EditText mEditTitle;
    @BindView(R.id.editBody)
    EditText mEditBody;
    
    private EditPostViewModel mPostDetailViewModel;
    private PostModel mPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityEditPostBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_post);
        ButterKnife.bind(this);

        mPost = (PostModel) getIntent().getSerializableExtra("post");
        if (mPost == null) {
            finish();
            return;
        }
        binding.setPost(mPost);

        mPostDetailViewModel = ViewModelProviders.of(this).get(EditPostViewModel.class);
        mPostDetailViewModel.getStatus().observe(this, this::onStatusChanged);
    }

    private void onStatusChanged(EditPostViewModel.Status status) {
        switch (status) {
            case TITLE_EMPTY:
                mEditTitle.setError("Insira um título");
                break;
            case BODY_EMPTY:
                mEditBody.setError("Insira um body");
                break;
            case EDIT_SUCCESS:
                Toast.makeText(this, "Post salvo com sucesso!", Toast.LENGTH_SHORT).show();
                finish();
                break;
            case EDIT_ERROR:
                Toast.makeText(this, "Erro ao salvar post, tente novamente.", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @OnClick(R.id.btnSave)
    protected void onSave() {
        mPostDetailViewModel.save(mPost.getUserId(), mPost.getId(), mEditTitle.getText().toString(), mEditBody.getText().toString());
    }
}
