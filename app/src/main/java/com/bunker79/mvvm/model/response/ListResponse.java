package com.bunker79.mvvm.model.response;

import com.bunker79.mvvm.model.PostModel;

import java.util.List;

public class ListResponse extends BaseResponse {

    private List<PostModel> list;

    public List<PostModel> getList() {
        return list;
    }

    public void setList(List<PostModel> list) {
        this.list = list;
    }
}
