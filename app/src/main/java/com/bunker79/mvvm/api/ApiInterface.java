package com.bunker79.mvvm.api;

import com.bunker79.mvvm.model.PostModel;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import rx.Observable;

public interface ApiInterface {

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @GET("posts")
    Observable<List<PostModel>> getPosts();

    @Headers({"Content-Type: application/json", "Accept: application/json"})
    @PUT("posts/{id}")
    Observable<PostModel> savePost(@Path("id") int id, @Body PostModel post_model);
}
