package com.bunker79.mvvm.api;

import android.content.Context;

import com.bunker79.mvvm.AppApplication;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

public enum ApiService {

    INSTANCE;

    private Retrofit mRetrofit;
    private OkHttpClient mOkHttpClient;
    private ApiInterface mApiInterface;

    ApiService() {
        Context ctx = AppApplication.getContext();
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ")
                .create();

        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();

        mOkHttpClient = okHttpBuilder.build();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(mOkHttpClient)
                .addCallAdapterFactory(rxAdapter);

        mRetrofit = builder.build();
    }

    public ApiInterface getApiInterface() {
        if (mApiInterface == null) mApiInterface = mRetrofit.create(ApiInterface.class);
        return mApiInterface;
    }
}
